import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import client from './apollo/ApolloClient';

import './App.css';

import Home from './pages/Home/Home';
import Teste from './pages/Teste/Teste';

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/teste" component={Teste} />
      </Router>
    </ApolloProvider>
  );
}

export default App;
