import React from 'react';
import useHomeContext from '../../contexts/Home';

function Currencys() {

    const { currencys } = useHomeContext();
    console.log(currencys);

    return (
        <section>
            <ul>
                {currencys.map((currency, i) => {
                    return <li key={i}>{currency.currency}</li>
                })}
            </ul>
        </section>
    );
}

export default Currencys;