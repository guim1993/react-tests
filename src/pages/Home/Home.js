import React from 'react';
import { useQuery } from '@apollo/react-hooks';

import { Provider as HomeContextProvider } from '../../contexts/Home';
import Currencys from '../../components/Currencys/Currencys';

import QueryCurrencys from './graphql/currencys.gql';

function Home() {

    const { data = {} } = useQuery(QueryCurrencys);

    return (
        <HomeContextProvider currencys={data.rates || []}>
            <section>
                Home
                <Currencys />
            </section>
        </HomeContextProvider>
    );
}

export default Home;