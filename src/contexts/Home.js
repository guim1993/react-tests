import React, { createContext, useContext } from 'react';

const HomeContext = createContext();

export function Provider(props) {
  const { children, currencys } = props;

  return (
    <HomeContext.Provider
      value={{
        currencys: currencys
      }}
    >
      {children}
    </HomeContext.Provider>
  );
}

export default function useHomeContext() {
  return useContext(HomeContext);
}
